<?php
session_start();

$text = $_POST['text'];
$pdo = new PDO("mysql:host=localhost:3306; dbname=myproject", "root", "");
$sql = "SELECT * FROM mytable WHERE  text=:text";
$statement = $pdo->prepare($sql);
$statement ->execute(['text'=>$text]);
$task = $statement->fetch(PDO::FETCH_ASSOC);

if(!empty($task)){
    $message = "Введенная запись уже присутствует в таблице.";
    $_SESSION['danger']=$message;
    header("Location: /task_10.php");
    exit;
}


$sql = "INSERT INTO mytable (text) values (:text)";
$statement = $pdo->prepare($sql);
$statement ->execute(['text'=>$text]);
$message = "Записано";
$_SESSION['success']=$message;

header("Location: /task_10.php");